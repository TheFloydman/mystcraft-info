package veovismuaddib.mystcraft_info.info;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import veovismuaddib.mystcraft_info.ModConfig;
import veovismuaddib.mystcraft_info.helpers.ModifierHelper;

import java.util.ArrayList;
import java.util.List;

public class LengthTooltip extends SymbolTooltip.SymbolTooltipGroup {
    public LengthTooltip() {
        super.setHeader("gui.mystcraft_info.length.header");
        super.add(new PercentLengthTooltip());
        super.add(new DecimalLengthTooltip());
    }

    @Override
    public List<String> getTooltip(IAgeSymbol symbol) {
        List<String> tooltip = super.getTooltip(symbol);

        if(!isVisible(symbol)) return tooltip;

        Number length = ModifierHelper.getLengthFromSymbol(symbol);
        if(length == null) return tooltip;

        return tooltip;
    }


    private static class PercentLengthTooltip extends SymbolTooltip {
        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.length_percent.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.length_percent.shift_required;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            Number length = ModifierHelper.getLengthFromSymbol(symbol);
            return length != null;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();
            Number length = ModifierHelper.getLengthFromSymbol(symbol);

            if(!isVisible(symbol)) return tooltip;
            if(length == null) return tooltip;

            tooltip.add(DEFAULT_FORMAT + String.format("%d%%", Math.round(length.floatValue() * 100)));

            return tooltip;
        }
    }

    private static class DecimalLengthTooltip extends SymbolTooltip {
        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.length_decimal.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.length_decimal.shift_required;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            Number length = ModifierHelper.getLengthFromSymbol(symbol);
            return length != null;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();
            Number length = ModifierHelper.getLengthFromSymbol(symbol);

            if(!isVisible(symbol)) return tooltip;
            if(length == null) return tooltip;

            tooltip.add(DEFAULT_FORMAT + String.format("%.1f", length.floatValue()));

            return tooltip;
        }
    }
}
