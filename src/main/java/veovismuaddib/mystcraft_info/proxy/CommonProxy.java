package veovismuaddib.mystcraft_info.proxy;

import com.xcompwiz.mystcraft.api.*;
import com.xcompwiz.mystcraft.api.exception.*;
import com.xcompwiz.mystcraft.api.hook.*;
import com.xcompwiz.mystcraft.api.symbol.*;
import net.minecraft.client.resources.I18n;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.*;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;
import veovismuaddib.mystcraft_info.*;
import veovismuaddib.mystcraft_info.info.*;
import veovismuaddib.mystcraft_info.listeners.*;

@Mod.EventBusSubscriber
public class CommonProxy {
    public PageAPI pageApi;
    public SymbolAPI symbolApi;
    public GrammarAPI grammarApi;
    public IForgeRegistry<IAgeSymbol> symbolRegistry;

    public void preInit(FMLPreInitializationEvent e) {
    }

    public void init(FMLInitializationEvent e) {
        try {
            ModMain.logger.info("Initializing Mystcraft APIs");
            pageApi = (PageAPI)MystObjects.entryPoint.getProviderInstance().getAPIInstance("page-1");
            symbolApi = (SymbolAPI)MystObjects.entryPoint.getProviderInstance().getAPIInstance("symbol-1");
            grammarApi = (GrammarAPI)MystObjects.entryPoint.getProviderInstance().getAPIInstance("grammar-1");
            symbolRegistry = GameRegistry.findRegistry(IAgeSymbol.class);
            ModMain.logger.info("Successfully initialized Mystcraft APIs");
        } catch(APIVersionRemoved e1) {
            ModMain.logger.error("API version removed!");
        } catch(APIVersionUndefined e2) {
            ModMain.logger.error("API version undefined!");
        } catch(APIUndefined e3) {
            ModMain.logger.error("API undefined!");
        }
    }

    public void postInit(FMLPostInitializationEvent e) {
        registerTooltipListener();
    }

    public String localize(String unlocalizedString, Object... parameters) {
        return I18n.format(unlocalizedString, parameters);
    }

    protected void registerTooltipListener() {
        PageTooltipListener tooltipListener = new PageTooltipListener();
        tooltipListener.addTooltip(new PoemTooltip());
        tooltipListener.addTooltip(new GrammarTooltip());
        tooltipListener.addTooltip(new ColorTooltip());
        tooltipListener.addTooltip(new LengthTooltip());
        tooltipListener.addTooltip(new DirectionTooltip());
        tooltipListener.addTooltip(new PhaseTooltip());
        tooltipListener.addTooltip(new ParentModTooltip());
        MinecraftForge.EVENT_BUS.register(tooltipListener);
    }
}
