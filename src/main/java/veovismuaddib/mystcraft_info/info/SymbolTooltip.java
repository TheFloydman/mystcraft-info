package veovismuaddib.mystcraft_info.info;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.text.TextFormatting;
import veovismuaddib.mystcraft_info.ModMain;

import java.util.ArrayList;
import java.util.List;

public abstract class SymbolTooltip {
    protected final String INDENT = "  ";
    protected final String DEFAULT_FORMAT = INDENT + TextFormatting.GRAY.toString();
    protected final String HEADER_FORMAT = TextFormatting.BLUE.toString() + TextFormatting.BOLD.toString();

    private String HEADER = null;

    private boolean leadingNewline = true;

    public abstract boolean isEnabled();

    public abstract boolean requiresShift();

    public boolean isVisible(IAgeSymbol symbol) {
        if(!isEnabled()) return false;
        if(!isRelevant(symbol)) return false;
        if(requiresShift() && !GuiScreen.isShiftKeyDown()) return false;

        return true;
    }

    public boolean isRelevant(IAgeSymbol symbol) {
        return true;
    }

    public void setLeadingNewline(boolean enabled) { leadingNewline = enabled; }

    public void setHeader(String unlocalizedHeader) {
        HEADER = ModMain.proxy.localize(unlocalizedHeader);
    }

    public List<String> getTooltip(IAgeSymbol symbol) {
        List<String> tooltip = new ArrayList<>();

        if(!isVisible(symbol)) return tooltip;

        if(leadingNewline) {
            tooltip.add("");
        }

        if(HEADER != null) {
            tooltip.add(HEADER_FORMAT + HEADER);
        }

        return tooltip;
    }

    public static abstract class SymbolTooltipGroup extends SymbolTooltip {
        private final List<SymbolTooltip> children = new ArrayList<>();

        public void add(SymbolTooltip child) {
            children.add(child);
        }

        @Override
        public boolean isEnabled() {
            for(SymbolTooltip child: children) {
                if(child.isEnabled()) return true;
            }

            return false;
        }

        @Override
        public boolean requiresShift() {
            for(SymbolTooltip child: children) {
                if(child.isEnabled() && child.requiresShift()) {
                    return true;
                }
            }

            return false;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            if(!isEnabled()) return false;
            for(SymbolTooltip child: children) {
                if(child.isRelevant(symbol)) return true;
            }
            return false;
        }

        @Override
        public boolean isVisible(IAgeSymbol symbol) {
            for(SymbolTooltip child: children) {
                if(child.isVisible(symbol)) return true;
            }
            return false;
        }

        @Override
        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = super.getTooltip(symbol);

            if(!isVisible(symbol)) return tooltip;

            for(SymbolTooltip child: children) {
                tooltip.addAll(child.getTooltip(symbol));
            }

            return tooltip;
        }

    }
}
