package veovismuaddib.mystcraft_info.info;

import java.util.ArrayList;
import java.util.List;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;

import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.ModContainer;
import veovismuaddib.mystcraft_info.ModConfig;
import veovismuaddib.mystcraft_info.helpers.ModifierHelper;

public class ParentModTooltip extends SymbolTooltip.SymbolTooltipGroup {

    protected static final String FORMAT_STANDARD = TextFormatting.RESET.toString() + TextFormatting.GRAY.toString();
    protected static final String FORMAT_MOD_SYMBOL = TextFormatting.RESET.toString() + TextFormatting.BLUE.toString()
            + TextFormatting.ITALIC.toString();
    protected static final String FORMAT_MOD_BLOCK = TextFormatting.RESET.toString()
            + TextFormatting.DARK_RED.toString() + TextFormatting.ITALIC.toString();
    protected static final String FORMAT_MOD_BIOME = TextFormatting.RESET.toString()
            + TextFormatting.DARK_GREEN.toString() + TextFormatting.ITALIC.toString();

    public ParentModTooltip() {
        super.add(new ParentModSymbolTooltip());
        super.add(new ParentModBlockTooltip());
        super.add(new ParentModSymbolAndBlockTooltip());
        super.add(new ParentModBiomeTooltip());
        super.add(new ParentModSymbolAndBiomeTooltip());
    }

    private static class ParentModSymbolTooltip extends SymbolTooltip implements IContextProvider {

        public ParentModSymbolTooltip() {
            this.setLeadingNewline(false);
        }

        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.parent_mod_symbol.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.parent_mod_symbol.shift_required;
        }

        @Override
        public boolean contextEnabled() {
            return ModConfig.tooltips.parent_mod_symbol.context_enabled;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();

            if(!isVisible(symbol))
                return tooltip;

            String modId = symbol.getRegistryName().getResourceDomain();
            String modName = getModName(modId);
            
            String tooltipString = contextEnabled() ? FORMAT_STANDARD + I18n.format("mystcraft_info.tooltips.prefix.parent_mod_symbol",
                    FORMAT_MOD_SYMBOL + modName + FORMAT_STANDARD) : FORMAT_MOD_SYMBOL + modName;
            
            tooltip = super.getTooltip(symbol);
            tooltip.add(tooltipString);

            return tooltip;
        }

    }

    private static class ParentModBlockTooltip extends SymbolTooltip implements IContextProvider {

        public ParentModBlockTooltip() {
            this.setLeadingNewline(false);
        }

        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.parent_mod_block.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.parent_mod_block.shift_required;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            IBlockState state = ModifierHelper.getBlockStateFromSymbol(symbol);
            return state != null;
        }

        @Override
        public boolean contextEnabled() {
            return ModConfig.tooltips.parent_mod_symbol.context_enabled;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();

            if(!isVisible(symbol))
                return tooltip;

            String modId = ModifierHelper.getBlockStateFromSymbol(symbol).getBlock().getRegistryName()
                    .getResourceDomain();
            String modName = getModName(modId);
            
            String tooltipString = contextEnabled() ? FORMAT_STANDARD + I18n.format("mystcraft_info.tooltips.prefix.parent_mod_block",
                    FORMAT_MOD_BLOCK + modName + FORMAT_STANDARD) : FORMAT_MOD_BLOCK + modName;
            
            tooltip = super.getTooltip(symbol);
            tooltip.add(tooltipString);

            return tooltip;
        }
    }

    private static class ParentModBiomeTooltip extends SymbolTooltip implements IContextProvider {

        public ParentModBiomeTooltip() {
            this.setLeadingNewline(false);
        }

        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.parent_mod_biome.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.parent_mod_biome.shift_required;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            Biome biome = ModifierHelper.getBiomeFromSymbol(symbol);
            return biome != null;
        }

        @Override
        public boolean contextEnabled() {
            return ModConfig.tooltips.parent_mod_biome.context_enabled;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();

            if(!isVisible(symbol))
                return tooltip;

            String modId = ModifierHelper.getBiomeFromSymbol(symbol).getRegistryName().getResourceDomain();
            String modName = getModName(modId);
            
            String tooltipString = contextEnabled() ? FORMAT_STANDARD + I18n.format("mystcraft_info.tooltips.prefix.parent_mod_biome",
                    FORMAT_MOD_BIOME + modName + FORMAT_STANDARD) : FORMAT_MOD_BIOME + modName;

            tooltip = super.getTooltip(symbol);
            tooltip.add(tooltipString);

            return tooltip;
        }
    }

    private static class ParentModSymbolAndBlockTooltip extends SymbolTooltip {

        public ParentModSymbolAndBlockTooltip() {
            this.setLeadingNewline(false);
        }

        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.parent_mod_symbol_and_block.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.parent_mod_symbol_and_block.shift_required;
        }
        
        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            IBlockState state = ModifierHelper.getBlockStateFromSymbol(symbol);
            return state != null;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();

            if(!isVisible(symbol))
                return tooltip;

            String modIdSymbol = symbol.getRegistryName().getResourceDomain();
            String modNameSymbol = getModName(modIdSymbol);

            IBlockState state = ModifierHelper.getBlockStateFromSymbol(symbol);
            String modIdBlock = "";
            if(state != null) {
                modIdBlock = ModifierHelper.getBlockStateFromSymbol(symbol).getBlock().getRegistryName()
                        .getResourceDomain();
            }
            String modNameBlock = getModName(modIdBlock);

            String tooltipString = FORMAT_STANDARD + I18n.format("mystcraft_info.tooltips.format.parent_mod_symbol_and_block",
                    FORMAT_MOD_SYMBOL + modNameSymbol + FORMAT_STANDARD,
                    FORMAT_MOD_BLOCK + modNameBlock + FORMAT_STANDARD);

            tooltip = super.getTooltip(symbol);
            tooltip.add(tooltipString);

            return tooltip;
        }
    }

    private static class ParentModSymbolAndBiomeTooltip extends SymbolTooltip {

        public ParentModSymbolAndBiomeTooltip() {
            this.setLeadingNewline(false);
        }

        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.parent_mod_symbol_and_biome.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.parent_mod_symbol_and_biome.shift_required;
        }
        
        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            Biome biome = ModifierHelper.getBiomeFromSymbol(symbol);
            return biome != null;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();

            if(!isVisible(symbol))
                return tooltip;

            String modIdSymbol = symbol.getRegistryName().getResourceDomain();
            String modNameSymbol = getModName(modIdSymbol);

            Biome biome = ModifierHelper.getBiomeFromSymbol(symbol);
            String modIdBiome = "";
            if(biome != null) {
                modIdBiome = ModifierHelper.getBiomeFromSymbol(symbol).getRegistryName().getResourceDomain();
            }
            String modNameBiome = getModName(modIdBiome);

            String tooltipString = FORMAT_STANDARD + I18n.format("mystcraft_info.tooltips.format.parent_mod_symbol_and_biome",
                    FORMAT_MOD_SYMBOL + modNameSymbol + FORMAT_STANDARD,
                    FORMAT_MOD_BIOME + modNameBiome + FORMAT_STANDARD);

            tooltip = super.getTooltip(symbol);
            tooltip.add(tooltipString);

            return tooltip;
        }
    }

    public static String getModName(String modId) {
        String modName = "Unknown";
        for(ModContainer mod : Loader.instance().getModList()) {
            if(mod.getModId().equals(modId)) {
                modName = mod.getName();
                break;
            }
        }
        return modName;
    }
}