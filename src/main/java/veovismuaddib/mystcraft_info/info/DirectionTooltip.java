package veovismuaddib.mystcraft_info.info;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import veovismuaddib.mystcraft_info.ModConfig;
import veovismuaddib.mystcraft_info.helpers.ModifierHelper;

import java.util.ArrayList;
import java.util.List;

public class DirectionTooltip extends SymbolTooltip.SymbolTooltipGroup {
    public DirectionTooltip() {
        super.setHeader("gui.mystcraft_info.direction.header");
        super.add(new PercentDirectionTooltip());
        super.add(new DegreeDirectionTooltip());
    }

    @Override
    public List<String> getTooltip(IAgeSymbol symbol) {
        List<String> tooltip = super.getTooltip(symbol);

        if(!isVisible(symbol)) return tooltip;

        Number direction = ModifierHelper.getDirectionFromSymbol(symbol);
        if(direction == null) return tooltip;

        return tooltip;
    }

    private static class PercentDirectionTooltip extends SymbolTooltip {
        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.direction_percent.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.direction_percent.shift_required;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            Number direction = ModifierHelper.getDirectionFromSymbol(symbol);
            return direction != null;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();
            Number direction = ModifierHelper.getDirectionFromSymbol(symbol);

            if(!isVisible(symbol)) return tooltip;
            if(direction == null) return tooltip;

            float percentageDirection = (direction.floatValue() / 360) % 1;

            tooltip.add(DEFAULT_FORMAT + String.format("%d%%", Math.round(percentageDirection * 100)));

            return tooltip;
        }
    }

    private static class DegreeDirectionTooltip extends SymbolTooltip {
        @Override
        public boolean isEnabled() {
            return ModConfig.tooltips.direction_degrees.enabled;
        }

        @Override
        public boolean requiresShift() {
            return ModConfig.tooltips.direction_degrees.shift_required;
        }

        @Override
        public boolean isRelevant(IAgeSymbol symbol) {
            Number direction = ModifierHelper.getDirectionFromSymbol(symbol);
            return direction != null;
        }

        public List<String> getTooltip(IAgeSymbol symbol) {
            List<String> tooltip = new ArrayList<>();
            Number direction = ModifierHelper.getDirectionFromSymbol(symbol);

            if(!isVisible(symbol)) return tooltip;
            if(direction == null) return tooltip;

            tooltip.add(DEFAULT_FORMAT + String.format("%.1f°", direction.floatValue()));

            return tooltip;
        }
    }
}
