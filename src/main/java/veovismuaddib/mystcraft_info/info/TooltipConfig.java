package veovismuaddib.mystcraft_info.info;

import net.minecraftforge.common.config.Config;

public class TooltipConfig {
    @Config.Comment("Display the color provided by a symbol as an RGB hexadecimal value")
    public Tooltip color_hex = new Tooltip(false, false);

    @Config.Comment("Display the color provided by a symbol as an RGB percentage")
    public Tooltip color_percent = new Tooltip(true, true);

    @Config.Comment("Display the direction provided by a symbol in degrees, North = 0/360 degrees")
    public Tooltip direction_degrees = new Tooltip(false, false);

    @Config.Comment("Display the direction provided by a symbol in percentage of a circle, North = 0/100%")
    public Tooltip direction_percent = new Tooltip(false, false);

    @Config.Comment("Display the grammar rules satisfied by a symbol")
    public Tooltip grammar = new Tooltip(true, false);

    @Config.Comment("Display the length provided by a symbol in percentage of a Minecraft day")
    public Tooltip length_percent = new Tooltip(true, false);

    @Config.Comment("Display the length provided by a symbol in decimal format, where 1.0 is equal to a Minecraft day")
    public Tooltip length_decimal = new Tooltip(false, false);

    @Config.Comment("Display the phase provided by a symbol in degrees, Nadir = 0/360 degrees")
    public Tooltip phase_degrees = new Tooltip(false, false);

    @Config.Comment("Display the phase provided by a symbol in percentage of a circle, Nadir = 0/100%")
    public Tooltip phase_percent = new Tooltip(false, false);

    @Config.Comment("Displays the words in a symbol's Narayan poem")
    public Tooltip poem = new Tooltip(true, false);

    @Config.Comment("Displays the mod that added a symbol")
    public TooltipWithContext parent_mod_symbol = new TooltipWithContext(true, false, true);

    @Config.Comment("Displays the mod that added the block for a symbol")
    public TooltipWithContext parent_mod_block = new TooltipWithContext(true, false, true);

    @Config.Comment("Displays the mod that added the biome for a symbol")
    public TooltipWithContext parent_mod_biome = new TooltipWithContext(true, false, true);

    @Config.Comment("Displays both the mod that added a symbol and the mod that added the block")
    public Tooltip parent_mod_symbol_and_block = new Tooltip(false, false);

    @Config.Comment("Displays both the mod that added a symbol and the mod that added the biome")
    public Tooltip parent_mod_symbol_and_biome = new Tooltip(false, false);

    public static class Tooltip {
        public boolean enabled;
        public boolean shift_required;

        private Tooltip(boolean defaultEnabled, boolean defaultShiftRequired) {
            this.enabled = defaultEnabled;
            this.shift_required = defaultShiftRequired;
        }
    }
    
    public static class TooltipWithContext {
        public boolean enabled;
        public boolean shift_required;
        public boolean context_enabled;

        private TooltipWithContext(boolean defaultEnabled, boolean defaultShiftRequired, boolean contextEnabled) {
            this.enabled = defaultEnabled;
            this.shift_required = defaultShiftRequired;
            this.context_enabled = contextEnabled;
        }
    }
}
