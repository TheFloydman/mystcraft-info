package veovismuaddib.mystcraft_info.info;

import com.xcompwiz.mystcraft.api.symbol.IAgeSymbol;
import net.minecraft.util.text.TextFormatting;
import veovismuaddib.mystcraft_info.ModConfig;
import veovismuaddib.mystcraft_info.helpers.PoemHelper;

import java.util.List;

public class PoemTooltip extends SymbolTooltip {
    @Override
    public boolean isEnabled() {
        return ModConfig.tooltips.poem.enabled;
    }

    @Override
    public boolean requiresShift() {
        return ModConfig.tooltips.poem.shift_required;
    }

    public PoemTooltip() {
        setLeadingNewline(false);
    }

    @Override
    public List<String> getTooltip(IAgeSymbol symbol) {
        List<String> tooltip = super.getTooltip(symbol);

        if(!isVisible(symbol)) return tooltip;

        List<String> poem = PoemHelper.getPoemFromSymbol(symbol);
        String formatting = DEFAULT_FORMAT + TextFormatting.ITALIC.toString();

        for(String word: poem) {
            tooltip.add(formatting + word);
        }

        return tooltip;
    }
}
